package co.classcalc.selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SignUp {
	// To verify the Sign Up Functionality
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://classcalc.com/scientific-calculator/share/zdQzPHJLobnRQt8JA/untitled-calc");
		driver.manage().window().maximize();
		// Using xpaths where proper IDs and classnames are not defined
		driver.findElement(By.className("sign_btn")).click();
		// Verify Title
		driver.findElement(By.className("popup_setRole_message"));
		//Select Role
		driver.findElement(By.className("popup_setRole_tab_area")).findElement(By.className("tab_1")).click();
		// Sign up with Email
		driver.findElement(By.xpath("(//button)[5]")).click();
		driver.findElement(By.xpath("(//input[@type='text'])[1]")).sendKeys("Quality");
		driver.findElement(By.xpath("(//input[@type='text'])[2]")).sendKeys("Assurance");
		driver.findElement(By.xpath("(//input[@type='text'])[3]")).sendKeys("asqadevsinc@gmail.com");
		driver.findElement(By.xpath("(//input[@id='signupPassword'])[1]")).sendKeys("Password99*");
		driver.findElement(By.xpath("(//input[@id='confimPassword'])[1]")).sendKeys("Password99*");
		driver.findElement(By.xpath("(//button)[4]")).click();
		// Sign Up with Google
		driver.findElement(By.className("popup_google_signin")).click();
		driver.findElement(By.id("identifierId")).sendKeys("calcQA@gmail.com");
		driver.findElement(By.id("identifierNext")).click();
		driver.findElement(By.id("password")).sendKeys("Password99*");
		driver.findElement(By.id("passwordNext")).click();
	}

}
