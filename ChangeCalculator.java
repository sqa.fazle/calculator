package co.classcalc.selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChangeCalculator {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://classcalc.com/scientific-calculator/share/zdQzPHJLobnRQt8JA/untitled-calc");
		driver.manage().window().maximize();
		// Using xpaths where proper IDs and classnames are not used
		driver.findElement(By.className("__classcalc-calc-options-controller")).click();
		driver.findElement(By.xpath("(//a[@role='link'])[1]")).click();
		driver.findElement(By.xpath("(//a[@role='link'])[2]")).click();
		driver.findElement(By.xpath("(//a[@role='link'])[3]")).click();
		driver.findElement(By.xpath("(//a[@role='link'])[4]")).click();
	}

}
