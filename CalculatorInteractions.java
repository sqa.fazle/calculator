package co.classcalc.selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalculatorInteractions {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://classcalc.com/scientific-calculator/share/zdQzPHJLobnRQt8JA/untitled-calc");
		driver.manage().window().maximize();
		// Using xpaths where proper IDs and classnames are not used
		
		// Verify the interaction with the calculator
		driver.findElement(By.xpath("(//button[@id='button-8'])[1]")).click();
		driver.findElement(By.xpath("(//button[@id='button-subtract'])[1]")).click();
		driver.findElement(By.xpath("(//button[@id='button-1'])[1]")).click();
		driver.findElement(By.xpath("(//button[@id='button-equals'])[1]")).click();
		
		// Verify reusability of values
		driver.findElement(By.xpath("(//span[@id='1647349723554'])[1]")).click();
		
		// Verify arrows to navigate between digits
		driver.findElement(By.id("__classcalc-left-right")).click();
		driver.findElement(By.id("__classcalc-arrow-right")).click();
		
		// Verify clear field
		driver.findElement(By.className("button-clear-all")).click();

	}

}
