package co.classcalc.selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Share {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://classcalc.com/scientific-calculator/share/zdQzPHJLobnRQt8JA/untitled-calc");
		driver.manage().window().maximize();
		// Using xpaths where proper IDs and classnames are not defined		
		driver.findElement(By.xpath("(//div[@class='taskbar-col-2 sc-ksYbfQ gyikpu'])[1]")).click();
		driver.findElement(By.xpath("__classcalc-share-calc-description")).sendKeys("Maths is now fun for everyone");
		// Copy share URL
		driver.findElement(By.xpath("__classcalc-share-calc-share-link")).click();
		// Embed Link
		driver.findElement(By.xpath("__classcalc-share-calc-embed-link")).click();

	}

}
