package co.classcalc.selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class HelpCenter {
	// To verify the Help Center
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		Actions actions = new Actions(driver);
		driver.get("https://classcalc.com/scientific-calculator/share/zdQzPHJLobnRQt8JA/untitled-calc");
		driver.manage().window().maximize();
		// Using xpaths where proper IDs and classnames are not defined
		actions.moveToElement(driver.findElement(By.className("dropdown-container")));
		// Verify Tutorial Links
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_image_title'])[1]")).click();
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_image_title'])[2]")).click();
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_image_title'])[3]")).click();
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_image_title'])[4]")).click();
		// Verify if all additional links are available
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_name'])[1]")).click();
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_name'])[2]")).click();
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_name'])[3]")).click();
		driver.findElement(By.className("dropdown-container")).findElement(By.xpath("(//div[@class='link_name'])[4]")).click();
	}

}
