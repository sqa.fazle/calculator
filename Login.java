package co.classcalc.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login {
	// To verify the login functionality
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://classcalc.com/scientific-calculator/share/zdQzPHJLobnRQt8JA/untitled-calc");
		driver.manage().window().maximize();
		// Using xpaths where proper IDs and classnames are not defined
		driver.findElement(By.className("login_btn")).click();
		//Sign In with google
		driver.findElement(By.className("popup_google_signin")).click();
		driver.findElement(By.id("identifierId")).sendKeys("calcQA@gmail.com");
		driver.findElement(By.id("identifierNext")).click();
		driver.findElement(By.id("password")).sendKeys("Password99*");
		driver.findElement(By.id("passwordNext")).click();
		//Sign in with Email
		driver.findElement(By.xpath("(//input[@type='text'])[1]")).sendKeys("calcQA@gmail.com");
		driver.findElement(By.xpath("(//input[@id='loginPassword'])[1]")).sendKeys("Password99*");
		driver.findElement(By.xpath("(//button)[4]")).click();	
	}

}
